# rdeps

**R deps** is a project to generate badges about dependencies of R packages.  

If you want badges for your project to be generated daily just make Pull Request adding your R package to the list of packages defined on top of [rdeps.R](./rdeps.R) file.  
Badges are refreshed daily.  

No external services are being employed in the process.  
Workflow is built using base R only. `BiocManager` is used only to get up-to-date Bioconductor repositories URLs.  

## Usage

markdown
```md
[![CRAN rev deps](https://jangorecki.gitlab.io/rdeps/data.table/CRAN_usage.svg?sanitize=true)](https://gitlab.com/jangorecki/rdeps)
```

html
```html
<a href="https://gitlab.com/jangorecki/rdeps"><img src="https://jangorecki.gitlab.io/rdeps/data.table/CRAN_usage.svg?sanitize=true"></a>
```

### Example on `knitr` package

[![total rev deps](https://jangorecki.gitlab.io/rdeps/knitr/direct_usage.svg?sanitize=true)](https://gitlab.com/jangorecki/rdeps) [![total recursive rev deps](https://jangorecki.gitlab.io/rdeps/knitr/indirect_usage.svg?sanitize=true)](https://gitlab.com/jangorecki/rdeps)

[![CRAN rev deps](https://jangorecki.gitlab.io/rdeps/knitr/CRAN_usage.svg?sanitize=true)](https://gitlab.com/jangorecki/rdeps) [![BioC rev deps](https://jangorecki.gitlab.io/rdeps/knitr/BioC_usage.svg?sanitize=true)](https://gitlab.com/jangorecki/rdeps)

[![Depends](https://jangorecki.gitlab.io/rdeps/knitr/Depends.svg?sanitize=true)](https://gitlab.com/jangorecki/rdeps) [![Imports](https://jangorecki.gitlab.io/rdeps/knitr/Imports.svg?sanitize=true)](https://gitlab.com/jangorecki/rdeps) [![LinkingTo](https://jangorecki.gitlab.io/rdeps/knitr/LinkingTo.svg?sanitize=true)](https://gitlab.com/jangorecki/rdeps) [![Suggests](https://jangorecki.gitlab.io/rdeps/knitr/Suggests.svg?sanitize=true)](https://gitlab.com/jangorecki/rdeps) [![Enhances](https://jangorecki.gitlab.io/rdeps/knitr/Enhances.svg?sanitize=true)](https://gitlab.com/jangorecki/rdeps)

[![Dependencies](https://jangorecki.gitlab.io/rdeps/knitr/dependencies.svg?sanitize=true)](https://gitlab.com/jangorecki/rdeps)

### Available badges (file names)

- total rev deps (including suggests): `direct_usage`
- total recursive rev deps (mandatory only): `indirect_usage`
- rev deps by repository (including suggests): `BioC_usage`, `CRAN_usage`
- rev deps by dependency relation: `Depends`, `Imports`, `LinkingTo`, `Suggests`, `Enhances`
- own recursive deps (mandatory only): `dependencies`
